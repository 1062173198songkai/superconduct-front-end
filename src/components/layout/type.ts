export interface MenuList{
    id: Number;
    pageHtml: String;
    name: String;
    list: MenuList[];
}