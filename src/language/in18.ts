import { createI18n } from "vue-i18n";
import messages from './index.ts';

const language = (navigator.language || 'zh').toLowerCase();
let lang = language || localStorage.getItem('language') || 'zh';

const i18n = createI18n({
    silentTranslationWarn: true,
    globalInjection: true,
    legacy: false,
    locale: lang,
    messages,
});

export default i18n;