import { createApp } from 'vue'
import './style.css'
import App from './App.vue';
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from "element-plus/es/locale/lang/zh-cn";
import i18n from './language/in18';
import router from "@/router";
const app = createApp(App);

app.use(ElementPlus,{
    locale: zhCn
}).use(i18n)
.use(router);

createApp(App).mount('#app');
